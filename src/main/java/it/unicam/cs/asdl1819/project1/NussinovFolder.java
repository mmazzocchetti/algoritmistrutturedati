/**
 *
 */
package it.unicam.cs.asdl1819.project1;

import java.util.Arrays;

/**
 * Implementazione dell'algoritmo di Nussinov per trovare, data una sequenza di
 * nucleotidi, una struttura secondaria senza pseudonodi che ha un numero
 * massimo di legami deboli.
 *
 * @author Luca Tesei
 *
 */
public class NussinovFolder implements FoldingAlgorithm {

    private final String primarySequence;

    // TODO inserire altre variabili istanza
    public Integer[][] matriceNussinov;

    public static void main(String[] args) {
        System.out.println("Start");
        NussinovFolder n = new NussinovFolder("GCACGACG");
        for (int row = 0; row < n.matriceNussinov.length; row++) {
            for (int col = 0; col < n.matriceNussinov[row].length; col++) {
                System.out.printf("%4d", n.matriceNussinov[row][col]);
            }
            System.out.println();
        }

    }

    /**
     * Costruisce un solver che utilizza l'algoritmo di Nussinov.
     *
     * @param primarySequence la sequenza di nucleotidi di cui fare il folding
     *
     * @throws IllegalArgumentException se la primarySequence contiene dei
     * codici di nucleotidi sconosciuti
     * @throws NullPointerException se la sequenza di nucleotidi è nulla
     */
    public NussinovFolder(String primarySequence) {
        if (primarySequence == null) {
            throw new NullPointerException(
                    "Tentativo di costruire un solutore Nussinov a partire da una sequenza nulla");
        }
        String seq = primarySequence.toUpperCase().trim();
        // check bases in the primary structure - IUPAC nucleotide codes
        for (int i = 0; i < seq.length(); i++) {
            switch (seq.charAt(i)) {
                case 'A':
                case 'U':
                case 'C':
                case 'G':
                    break;
                default:
                    throw new IllegalArgumentException(
                            "INPUT ERROR: primary structure contains an unkwnown nucleotide code at position "
                            + (i + 1));
            }
        }
        this.primarySequence = seq;

        // TODO continuare le operazioni del costruttore
        //1. Istanzio la matrice di Nussinov
        this.matriceNussinov = new Integer[seq.length()][seq.length() + 1];
        //2. Prima condizione dell'algoritmo
        for (int i = 1; i < seq.length(); i++) {
            this.matriceNussinov[i][i - 1] = 0;
        }
        //3. Chiamo l'algoritmo ricorsivo per popolare la matrice, passando i valori 0 ed n-1
        nussinovAlgo(0, seq.length() - 1);
    }

    private int nussinovAlgo(int i, int j) {

        // 1. Se il valore è diverso dal mio valore nullo allora
        if (this.matriceNussinov[i][j] != null) {
            return this.matriceNussinov[i][j];
        }

        if (i >= j) {
            this.matriceNussinov[i][j] = 0;
            return 0;
        }

        int max = 0;

        max = Math.max(max, Math.max(nussinovAlgo(i + 1, j), nussinovAlgo(i, j - 1)));

        if (controllaCoppieConsentita(i, j)) {
            max = Math.max(max, nussinovAlgo(i + 1, j - 1) + 1);
        }

        for (int k = i + 1; k < j; k++) {
            max = Math.max(max, nussinovAlgo(i, k) + nussinovAlgo(k + 1, j));
        }

        this.matriceNussinov[i][j] = max;
        return max;
    }

    private Boolean controllaCoppieConsentita(int k, int j) {
        char a = this.primarySequence.charAt(k);
        char c = this.primarySequence.charAt(j);
        switch (a) {
            case 'A':
                return c == 'U';
            case 'U':
                return c == 'A' || c == 'G';
            case 'C':
                return c == 'G';
            case 'G':
                return c == 'U' || c == 'C';
        }
        return false;
    }

    public String getName() {
        return "NussinovFolder";
    }

    @Override
    public String getSequence() {
        return this.primarySequence;
    }

    @Override
    public SecondaryStructure getOneOptimalStructure() {
        // TODO implementare
        return null;
    }

    @Override
    public void fold() {
        // TODO implementare
    }

    @Override
    public boolean isFolded() {
        // TODO implementare
        return false;
    }

}
