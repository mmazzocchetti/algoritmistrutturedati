package it.unicam.cs.asdl1819.project1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Un oggetto di questa classe rappresenta una struttura secondaria di RNA.
 *
 * @author Luca Tesei
 *
 */
public class SecondaryStructure {

    private final String primarySequence;

    private Set<WeakBond> bonds;

    private Integer pseudoKnot = 0;

    private Integer primarySequenceLenght;
    private char[] secondarySequenceString;

    /**
     * Costruisce una struttura secondaria con un insieme vuoto di legami
     * deboli.
     *
     * @param primarySequence la sequenza di nucleotidi
     *
     * @throws IllegalArgumentException se la primarySequence contiene dei
     * codici di nucleotidi sconosciuti
     * @throws NullPointerException se la sequenza di nucleotidi è nulla
     */
    public SecondaryStructure(String primarySequence) {
        if (primarySequence == null) {
            throw new NullPointerException(
                    "Tentativo di costruire un solutore Nussinov a partire da una sequenza nulla");
        }
        String seq = primarySequence.toUpperCase().trim();
        // check bases in the primary structure
        for (int i = 0; i < seq.length(); i++) {
            switch (seq.charAt(i)) {
                case 'A':
                case 'U':
                case 'C':
                case 'G':
                    break;
                default:
                    throw new IllegalArgumentException(
                            "INPUT ERROR: primary structure contains an unkwnown nucleotide code at position "
                            + (i + 1));
            }
        }
        this.primarySequence = seq;
        this.primarySequenceLenght = seq.length();
        this.secondarySequenceString = new char[seq.length()];
        Arrays.fill(this.secondarySequenceString, '.');
        this.bonds = new HashSet<>();
    }

    /**
     * Costruisce una struttura secondaria con un insieme dato di legami deboli.
     *
     * @param primarySequence la sequenza di nucleotidi
     * @param bonds l'insieme dei legami deboli presenti nella struttura
     *
     * @throws IllegalArgumentException se la primarySequence contiene dei
     * codici di nucleotidi sconosciuti
     * @throws NullPointerException se la sequenza di nucleotidi è nulla
     * @throws NullPointerException se l'insieme dei legami è nullo
     * @throws IndexOutOfBoundsException se almeno uno dei due indici di uno dei
     * legami deboli passati esce fuori dai limiti della sequenza primaria di
     * questa struttura
     * @throws IllegalArgumentException se almeno uno dei legami deboli passati
     * connette due nucleotidi a formare una coppia non consentita.
     *
     */
    public SecondaryStructure(String primarySequence, Set<WeakBond> bonds) {
        if (primarySequence == null) {
            throw new NullPointerException(
                    "Tentativo di costruire un solutore Nussinov a partire da una sequenza nulla");
        }
        String seq = primarySequence.toUpperCase().trim();
        // check bases in the primary structure
        for (int i = 0; i < seq.length(); i++) {
            switch (seq.charAt(i)) {
                case 'A':
                case 'U':
                case 'C':
                case 'G':
                    break;
                default:
                    throw new IllegalArgumentException(
                            "INPUT ERROR: primary structure contains an unkwnown nucleotide code at position "
                            + (i + 1));
            }
        }
        this.primarySequence = seq;
        this.primarySequenceLenght = seq.length();
        this.secondarySequenceString = new char[seq.length()];
        Arrays.fill(this.secondarySequenceString, '.');
        this.bonds = new HashSet<>();
        for (WeakBond b : bonds) {
            this.addBond(b);
        }
    }

    /**
     * Restituisce la sequenza di nucleotidi di questa struttura secondaria.
     *
     * @return la sequenza di nucleotidi di questa struttura secondaria
     */
    public String getPrimarySequence() {
        return this.primarySequence;
    }

    /**
     * Restituisce l'insieme dei legami deboli di questa struttura secondaria.
     *
     * @return l'insieme dei legami deboli di questa struttura secondaria
     */
    public Set<WeakBond> getBonds() {
        return this.bonds;
    }

    /**
     * Determina se questa struttura contiene pseudonodi.
     *
     * @return true, se in questa struttura ci sono almeno due legami deboli che
     * si incrociano, false altrimenti
     */
    public boolean isPseudoknotted() {
        // TODO implementare
        return this.pseudoKnot > 0;
    }

    /**
     * Aggiunge un legame debole a questa struttura.
     *
     * @param b il legame debole da aggiungere
     * @return true se il legame è stato aggiunto, false se era già presente
     *
     * @throws NullPointerException se il legame passato è nullo
     * @throws IndexOutOfBoundsException se almeno uno uno dei due indici del
     * legame debole passato esce fuori dai limiti della sequenza primaria di
     * questa struttura
     * @throws IllegalArgumentException se il legame debole passato connette due
     * nucleotidi a formare una coppia non consentita.
     */
    public boolean addBond(WeakBond b) {
        // TODO implementare
        //Il legame passato è nullo?
        if (b == null) {
            throw new NullPointerException("Impossibile passare un legame nullo");
        }

        if (b.getI() > this.primarySequenceLenght || b.getJ() > this.primarySequenceLenght) {
            throw new IndexOutOfBoundsException("Uno uno dei due indici del\n"
                    + "     * legame debole passato esce fuori dai limiti della sequenza primaria di\n"
                    + "     * questa struttura");

        }
        if (!controllaCoppieConsentita(b)) {
            throw new IllegalArgumentException("Il legame debole passato connette due\n"
                    + "     * nucleotidi a formare una coppia non consentita.");
        }

        //Se è vuoto lo inserisco senza controlli
        //Controllo se ci sono pseudonodi
        for (WeakBond w : this.bonds) {
            int i = b.getI();
            int j = b.getJ();
            int i2 = w.getI();
            int j2 = w.getJ();
            if (i2 < i && i < j && j < j2) {
                continue;
            }
            if (i < i2 && i2 < j2 && j2 < j) {
                continue;
            }
            if (j2 < i) {
                continue;
            }
            if (j < i2) {
                continue;
            }
            //Se non verifica una delle condizioni sopra allora aggiungo uno pseudonodo
            this.pseudoKnot = this.pseudoKnot + 1;
        }
        this.bonds.add(b);
        this.secondarySequenceString[b.getI()] = '(';
        this.secondarySequenceString[b.getJ()] = ')';
        //Vuol dire non ci sono state eccezioni
        return true;
    }

    private Boolean controllaCoppieConsentita(WeakBond b) {
        char a = this.primarySequence.charAt(b.getI());
        char c = this.primarySequence.charAt(b.getJ());
        switch (a) {
            case 'A':
                return c == 'U';
            case 'U':
                return c == 'A' || c == 'G';
            case 'C':
                return c == 'G';
            case 'G':
                return c == 'U' || c == 'C';
        }
        return false;
    }

    /**
     * Restituisce il numero di legami deboli presenti in questa struttura.
     *
     * @return il numero di legami deboli presenti in questa struttura
     */
    public int getCardinality() {
        return this.bonds.size();
    }

    /**
     * Restituisce una stringa contenente la rappresentazione nella notazione
     * dot-bracket di questa struttura secondaria.
     *
     * @return una stringa contenente la rappresentazione nella notazione
     * dot-bracket di questa struttura secondaria
     *
     * @throws IllegalStateException se questa struttura secondaria contiene
     * pseudonodi
     */
    public String getDotBracketNotation() {
        // TODO implementare
        String output = "";
        String primary = "";
        String secondary = "";
        String tempSecondary = new String(this.secondarySequenceString);
        if (this.primarySequence.length() < 51) {
            output = this.primarySequence + System.lineSeparator() + new String(this.secondarySequenceString);
        } else {
            for (int i = 0; i < this.primarySequenceLenght; i = i + 50) {
                primary = primary + this.primarySequence.substring(i, i + 50) + System.lineSeparator();
                secondary = secondary + tempSecondary.substring(i, i + 50) + System.lineSeparator();
            }
            output = primary + secondary;
        }
        return output;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bonds == null) ? 0 : bonds.hashCode());
        result = prime * result
                + ((primarySequence == null) ? 0 : primarySequence.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SecondaryStructure)) {
            return false;
        }
        SecondaryStructure other = (SecondaryStructure) obj;
        if (bonds == null) {
            if (other.bonds != null) {
                return false;
            }
        } else if (!bonds.equals(other.bonds)) {
            return false;
        }
        if (primarySequence == null) {
            if (other.primarySequence != null) {
                return false;
            }
        } else if (!primarySequence.equals(other.primarySequence)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        Iterator<WeakBond> i = this.bonds.iterator();
        if (i.hasNext()) {
            WeakBond current = i.next();
            while (i.hasNext()) {
                WeakBond next = i.next();
                sb.append(current.toString() + ", ");
                if (!i.hasNext()) {
                    sb.append(next.toString());
                    break;
                }
                current = next;
            }
        }
        sb.append("}");
        return sb.toString();
    }

}
